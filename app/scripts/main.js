$(document).ready(function() {

    $('.single-item').slick({
        arrows: false,
        dots:true,
        autoplay: true,
        speed: 2000,
        autoplaySpeed: 2000,
        focusOnSelect: true
    });

    $('.why-slider').slick({
        dots:false,
        arrows: true,
        prevArrow:'<div class="prev btn border rounded bg-blue text-white"><i class="fas fa-angle-left"></i></div>',
        nextArrow:'<div class="next btn border rounded bg-blue text-white"><i class="fas fa-angle-right"></i></div>',
        slidesToShow: 3,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              dots:true,
              adaptiveHeight: true,
              slidesToShow: 1
            }
          }
        ]
      });
   function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }
    // With JQuery
    $('#ex1').slider({
        formatter: function(value) {
            return formatNumber(value);
        }
    });
    
    $('#ex12c').slider({ id: 'slider12c', min: 0, max: 500000, range: true, value: [200000, 300000],formatter: function(value) {
      var value1=value[0];
      console.log('var 1'+value1);
      if(!value1){
        value1=0;
      }
      var value2=value[1];
      if(!value2){
        value2=0;
      }
        return formatNumber(value1)+':'+formatNumber(value2);
    } });

});